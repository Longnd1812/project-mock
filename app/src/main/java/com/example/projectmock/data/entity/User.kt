package com.example.projectmock.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.projectmock.data.utils.DatabaseConstants.PASSWORD
import com.example.projectmock.data.utils.DatabaseConstants.USERNAME
import com.example.projectmock.data.utils.DatabaseConstants.USER_ID
import com.example.projectmock.data.utils.DatabaseConstants.USER_TABLE

@Entity(tableName = USER_TABLE)
data class User(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = USER_ID)
    var userId : Int = 0,

    @ColumnInfo(name = USERNAME)
    var username : String,

    @ColumnInfo(name = PASSWORD)
    var password : String
)
