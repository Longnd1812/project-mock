package com.example.projectmock.data.relationship

import androidx.room.Embedded
import androidx.room.Relation
import com.example.projectmock.data.entity.Run
import com.example.projectmock.data.entity.User
import com.example.projectmock.data.utils.DatabaseConstants.USER_ID

data class UserAndRun(
    @Embedded
    val user: User,
    @Relation(
        parentColumn = USER_ID,
        entityColumn = USER_ID
    )
    val run : List<Run>
)   