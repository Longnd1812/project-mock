package com.example.projectmock.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.projectmock.data.entity.Run
import com.example.projectmock.data.entity.User
import com.example.projectmock.data.utils.DatabaseConstants.RUN_DB

@Database(entities = [User::class, Run::class], version = 1)
abstract class RunDatabase : RoomDatabase() {
    abstract val runDAO: DAO

    companion object {
        @Volatile
        private var INSTANCE: RunDatabase? = null

        fun getInstance(context: Context): RunDatabase = synchronized(this) {
            return INSTANCE ?: Room.databaseBuilder(
                context.applicationContext,
                RunDatabase::class.java,
                RUN_DB,
            ).build().also {
                INSTANCE = it
            }
        }
    }


}