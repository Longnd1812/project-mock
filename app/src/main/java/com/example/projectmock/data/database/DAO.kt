package com.example.projectmock.data.database

import androidx.room.*
import com.example.projectmock.data.entity.Run
import com.example.projectmock.data.entity.User

@Dao
interface DAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRun(vararg run: Run)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRun(vararg user: User)

    @Delete
    suspend fun delete(run: Run)

    @Delete
    suspend fun delete(user: User)

    @Query("SELECT * FROM Run ORDER BY tim_stamp ")
    fun getAllRunsSortedByDate(): List<Run>

    @Query("SELECT * FROM Run ORDER BY Time_run ")
    fun getAllRunsSortedByTime(): List<Run>

    @Query("SELECT * FROM Run ORDER BY calories_burned ")
    fun getAllRunsSortedByCaloriesBurned(): List<Run>

    @Query("SELECT * FROM Run ORDER BY avgSpeed_kmh ")
    fun getAllRunsSortedByAvgSpeed(): List<Run>

    @Query("SELECT * FROM Run ORDER BY `distance_meters ` ")
    fun getAllRunsSortedDistance(): List<Run>
}