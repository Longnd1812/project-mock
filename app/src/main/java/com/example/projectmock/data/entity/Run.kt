package com.example.projectmock.data.entity

import androidx.room.*
import com.example.projectmock.data.utils.DatabaseConstants.RUN_TABLE
import com.example.projectmock.data.utils.DatabaseConstants.RUN_ID
import com.example.projectmock.data.utils.DatabaseConstants.USER_ID
import com.example.projectmock.data.utils.DatabaseConstants.ADDRESS
import com.example.projectmock.data.utils.DatabaseConstants.TIME_RUN
import com.example.projectmock.data.utils.DatabaseConstants.TIME_START
import com.example.projectmock.data.utils.DatabaseConstants.AVERAGE_SPEED
import com.example.projectmock.data.utils.DatabaseConstants.DISTANCE
import com.example.projectmock.data.utils.DatabaseConstants.BURNING_CALORIES


@Entity(
    tableName = RUN_TABLE,
    foreignKeys = [ForeignKey(entity = User::class,
    parentColumns = arrayOf(USER_ID),
    childColumns = arrayOf(USER_ID),
    onDelete = ForeignKey.CASCADE)])

data class Run(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = RUN_ID)
    var runId : Int? = 0,

    @ColumnInfo(name = ADDRESS)
    var address_gps : String? = null,

    @ColumnInfo(name = TIME_START)
    var tim_stamp : Long? = 0L,

    @ColumnInfo(name = AVERAGE_SPEED)
    var avgSpeed_kmh : Float? = 0F,

    @ColumnInfo(name = DISTANCE)
    var distance_meters : Int? = 0,

    @ColumnInfo(name = TIME_RUN)
    var time_run : Long? = 0L,

    @ColumnInfo(name = BURNING_CALORIES)
    var calories_burned : Int? =0,

    @ColumnInfo(name = USER_ID)
    var userId : Int
)