package com.example.projectmock.data.utils

object DatabaseConstants {
    const val RUN_DB  = "run_db"
    const val RUN_TABLE = "run"
    const val USER_TABLE = "user"
    const val ADDRESS = "address_gps"
    const val TIME_START = "tim_stamp"
    const val AVERAGE_SPEED = "avgSpeed_kmh"
    const val DISTANCE = "distance_meters "
    const val TIME_RUN = "time_run"
    const val BURNING_CALORIES = "calories_burned"
    const val RUN_ID = "run_id"
    const val USERNAME = "username"
    const val PASSWORD = "password"
    const val USER_ID  = "user_id"
}